package com.pandri.execute;

import java.util.Scanner;

public class Execute {

	public static void main(String[] args) {
		System.out.println("Insert row(s)?");
		Scanner is = new Scanner(System.in);
		try {
			Integer row = is.nextInt();
			for (int i = 1; i <= row; i++) {
				for (int j = row; j > 0; j--) {
					if (i >= j) {
						System.out.print("#");
					} else {
						System.out.print(" ");
					}
				}
				System.out.println("");
			}
			
		} catch (Exception e) {
			System.err.println("Please insert numeric value.");
		}
		
		is.close();
	}

}
